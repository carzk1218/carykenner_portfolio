let cp1 = document.getElementById("cp1");
let dp = document.getElementById("dp");
let bg = document.getElementById("bg");
let ds = document.getElementById("description");

cp1.addEventListener("mouseover", function () {
  dp.src = "./assets/images/geekseek.jpg";
  bg.style.backgroundImage =
    'linear-gradient(90deg, rgba(0, 0, 0, 0.7455357143) 50%, rgba(255, 255, 255, 0.0396533613) 100%, rgba(0, 212, 255, 1) 100%), url("./assets/images/bg1.jpg")';
  dp.setAttribute("class", "trans");
  ds.setAttribute("class", "trans");
  ds.innerHTML =
    "<h1>Geek Seek</h1> An online static newspaper website where you can get all the latest news about gaming, movies, events, anime, tv series, etc. Used HTML5, CSS3, Sass, Bootstrap, JavaScript, and jQuery to build a responsive website.";
});

cp1.addEventListener("mouseleave", function () {
  dp.src = "./assets/images/carykenner.jpg";
  bg.style.backgroundImage =
    'linear-gradient(90deg, rgba(0, 0, 0, 0.7455357143) 50%, rgba(255, 255, 255, 0.0396533613) 100%, rgba(0, 212, 255, 1) 100%), url("https://images.wallpaperscraft.com/image/night_city_bw_full_moon_128768_2560x1024.jpg")';
  dp.classList.remove("trans");
  ds.classList.remove("trans");
  ds.innerHTML = "";
});

cp2.addEventListener("mouseover", function () {
  dp.src = "./assets/images/cc.jpg";
  bg.style.backgroundImage =
    'linear-gradient(90deg, rgba(0, 0, 0, 0.7455357143) 50%, rgba(255, 255, 255, 0.0396533613) 100%, rgba(0, 212, 255, 1) 100%), url("./assets/images/bg2.jpg")';
  dp.setAttribute("class", "trans");
  ds.setAttribute("class", "trans");
  ds.innerHTML =
    "<h1>Costume City</h1> An eCommerce website built to sell costume needs especially for events, with an easy to use asset management platform for the administrator to keep track of their sales, assets, and customer interactions using Laravel and MySQL. Hosted the application via Heroku & db4free. To login using admin rights, please use user: admin@admin.com pass: mrkennerdy1218";
});

cp2.addEventListener("mouseleave", function () {
  dp.src = "./assets/images/carykenner.jpg";
  bg.style.backgroundImage =
    'linear-gradient(90deg, rgba(0, 0, 0, 0.7455357143) 50%, rgba(255, 255, 255, 0.0396533613) 100%, rgba(0, 212, 255, 1) 100%), url("https://images.wallpaperscraft.com/image/night_city_bw_full_moon_128768_2560x1024.jpg")';
  dp.classList.remove("trans");
  ds.classList.remove("trans");
  ds.innerHTML = "";
});
